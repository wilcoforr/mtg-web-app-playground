﻿using System;
using System.Collections.Generic;

namespace MtgWebAppPlayground.Models
{
    /// <summary>
    /// A Set is an expansion release of around 300 cards
    /// </summary>
    public class Set
    {
        public string Name { get; set; }
        public List<Card> Cards { get; set; }
        public DateTime ReleaseDate { get; set; }
    }
}
