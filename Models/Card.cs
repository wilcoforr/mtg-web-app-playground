﻿using System.Collections.Generic;

namespace MtgWebAppPlayground.Models
{
    /// <summary>
    /// A Card object - implements some, but not all, properties of the mtgjson card object definition
    /// https://mtgjson.com/structures/card/
    /// Pretty much all of these properties are strings, because for example Power can be * for a varying amount
    /// </summary>
    public class Card
    {
        public string Name { get; set; }
        public List<string> Colors { get; set; }
        public string Artist { get; set; }
        public string Text { get; set; }
        public string FlavorText { get; set; }
        public string Power { get; set; }
        public string ManaCost { get; set; }
        public string Type { get; set; }
        public string Toughness { get; set; }
        public string Rarity { get; set; }
    }
}
