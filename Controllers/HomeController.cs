﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Text.Json;
using System.Net.Http;

using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

using MtgWebAppPlayground.Models;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Infrastructure;

namespace MtgWebAppPlayground.Controllers
{
    public class HomeController : Controller
    {
        //one way to view logger information is via the console by starting the project with `dotnet run`
        private readonly ILogger<HomeController> _logger;

        //"HttpClient is intended to be instantiated once and re-used throughout the life of an application.
        //Instantiating an HttpClient class for every request will exhaust the number of sockets available
        //under heavy loads. This will result in SocketException errors. Below is an example 
        //using HttpClient correctly." - microsoft docs
        //https://docs.microsoft.com/en-us/dotnet/api/system.net.http.httpclient?view=netcore-3.1
        private static readonly HttpClient _httpClient;


        static HomeController()
        {
            _httpClient = new HttpClient();
        }

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        /// <summary>
        /// Show a list of cards for an expansion.
        /// </summary>
        /// <param name="expansionChoice">
        /// The expansion Set to display. 
        /// By default, showing THB - Theros Beyond Death - released January 17, 2020
        /// </param>
        /// <returns>A View displaying the selected expansion's cards.</returns>
        [HttpGet]
        public async Task<IActionResult> Index(string expansionChoice = "ZNR")
        {
            try
            {
                _logger.LogInformation($"User requested expansion code: {expansionChoice}");

                string url = $"https://www.mtgjson.com/json/{expansionChoice}.json";

                var uriJsonTarget = new Uri(url);

                string jsonString = await _httpClient.GetStringAsync(uriJsonTarget);

                var options = new JsonSerializerOptions { PropertyNameCaseInsensitive = true };

                Set set = JsonSerializer.Deserialize<Set>(jsonString, options);

                //remove "duplicate" cards like cards that have extended art/different art, 
                //but keep the card if the artists differ.
                set.Cards = set.Cards
                    .GroupBy(c => new { c.Name, c.Artist })
                    .Select(c => c.First())
                    .ToList();

                _logger.LogInformation($"User received: {set.Name} for a total of {set.Cards.Count} cards.");

                return View(set);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error!", null);

                return RedirectToAction("Error");
            }
        }

        public async Task<IActionResult> GenerateBoosterPack(string expansionChoice = "ZNR")
        {
            string url = $"https://www.mtgjson.com/json/{expansionChoice}.json";

            var uriJsonTarget = new Uri(url);

            string jsonString = await _httpClient.GetStringAsync(uriJsonTarget);

            var options = new JsonSerializerOptions { PropertyNameCaseInsensitive = true };

            Set set = JsonSerializer.Deserialize<Set>(jsonString, options);

            var booster = new List<Card>();

            // .ToList() so we can use indexing for getting random cards
            var lands = set.Cards.Where(c => c.Type.Contains("Basic Land")).ToList();
            var commons = set.Cards.Where(c => c.Rarity == "common" && !c.Type.Contains("Basic Land")).ToList();
            var uncommons = set.Cards.Where(c => c.Rarity == "uncommon").ToList();
            var rares = set.Cards.Where(c => c.Rarity == "rare").ToList();
            var mythics = set.Cards.Where(c => c.Rarity == "mythic").ToList(); //make getting one mythic out of 8 rares - so like if 8 packs opened, 1 mythic, 7 rares.

            var random = new Random();

            //real booster has one land, 10 common, 3 uncommon, 1 rare or mythic rare
            for (int i = 0; i < 15; i++)
            {
                if (i == 0)
                {
                    booster.Add(lands[random.Next(0, lands.Count - 1)]);
                }
                else if (i < 11)
                {
                    booster.Add(commons[random.Next(0, commons.Count - 1)]);
                }
                else if (i < 14)
                {
                    booster.Add(uncommons[random.Next(0, uncommons.Count - 1)]);
                }
                else
                {
                    // 1/8th chance to get a mythic rare
                    if (random.Next(0, 8) == 0)
                    {
                        booster.Add(mythics[random.Next(0, mythics.Count - 1)]);
                    }
                    else
                    {
                        booster.Add(rares[random.Next(0, rares.Count - 1)]);
                    }
                }
            }

            return View(booster);
        }


        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error(string message = "")
        {
            var errorVM = new ErrorViewModel
            {
                RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier,
                Message = message
            };

            return View(errorVM);
        }
    }
}
