﻿# MTG Web App Playground

A basic ASP.NET Core 3.1 MVC web application that downloads JSON data from [MTGJson.com](https://mtgjson.com) and displays a list of a cards for an expansion.

Note: So far, Base/core set editions have not been added.



